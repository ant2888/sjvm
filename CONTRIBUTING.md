The general work flow for this project is as follows:

* Create a branch for the issue you are working on

* Mark the issue in-progress and move it into to the board

* Create a PR and wait for it to be merged

Note that typically any new feature should have a complementing test (or tests)
added to the test suite.