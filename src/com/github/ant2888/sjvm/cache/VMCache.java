package cache;

import org.codehaus.jackson.map.ObjectMapper;
import utils.ConfigLoader;

import java.io.*;
import java.nio.file.Files;
import java.util.Enumeration;
import java.util.jar.*;

/**
 * Class used to keep track of and cache any/all JAR's
 */
public class VMCache {

    private static VMCache singleton;
    private ProgramFileSchema fileMap;
    private ObjectMapper mapper;

    private VMCache() {
        initializeDirectories();
        initializePrograms();
        VMCache.singleton = this;
    }

    /**
     * Initializes and ensures programs from the programs json file
     */
    private void initializePrograms() {
        String root = ConfigLoader.STORAGE_DIR;
        String jars = root + "/jars";
        String unpacked = root + "/unpacked";

        mapper = new ObjectMapper();

        try {
            fileMap = mapper.readValue(new File(root + "/programs.json"),
                    ProgramFileSchema.class);

            if (fileMap == null || fileMap.programs == null) return;
        } catch (IOException e) {
            System.err.println("[CRITICAL ERROR] Failed to read programs json: "+e);
            System.exit(3);
        }

        for (ProgramSchema ps : fileMap.programs) {
            File jarDir = new File(jars+"/"+ps.name);
            File unpDir = new File(unpacked+"/"+ps.name);

            if (Files.notExists(jarDir.toPath())) {
                System.err.println("[WARNING ERROR] Error in config. No jar "+ps.name+" found");
                continue;
            }

            if (Files.notExists(unpDir.toPath())) {
                // need to unpack the jar
                try {
                    VMCache.unpackJar(ps.name);
                } catch (IOException e) {
                    System.err.println("[WARNING ERROR] Error trying to unpack jar "+ps.name);
                }
            }
        }
    }

    /**
     * Unpacks the jar file into the respective directory for use in the server
     * @param name The file  name  of the jar (note this should be location storage_dir/jar/)
     * @throws IOException Failed to unpack
     */
    public static void unpackJar(String name) throws IOException {
        String root = ConfigLoader.STORAGE_DIR;
        String dest = root + "/unpacked/"+name;

        JarFile jf = new JarFile(new File(root+"/jars/"+name+"/"+name));
        // fist get all directories,
        // then make those directory on the destination Path
        for (Enumeration<JarEntry> enums = jf.entries(); enums.hasMoreElements();) {
            JarEntry entry = (JarEntry) enums.nextElement();

            String fileName = dest + File.separator + entry.getName();
            File f = new File(fileName);

            if (fileName.endsWith("/")) {
                f.mkdirs();
            }
        }

        //now create all files
        for (Enumeration<JarEntry> enums = jf.entries(); enums.hasMoreElements();) {
            JarEntry entry = (JarEntry) enums.nextElement();

            String fileName = dest + File.separator + entry.getName();
            File f = new File(fileName);

            if (!fileName.endsWith("/")) {
                InputStream is = jf.getInputStream(entry);
                FileOutputStream fos = new FileOutputStream(f);

                // write contents of 'is' to 'fos'
                while (is.available() > 0) {
                    fos.write(is.read());
                }

                fos.close();
                is.close();
            }
        }
    }

    /**
     * Adds a program to the programs json file and attempts to decode it
     * @param name Name of the program i.e test.jar
     * @param main Name of the main class in the file i.e- Main.class
     * @throws VMCacheException In case of error via IOException or bad name
     */
    public void addProgram(String name, String main) throws VMCacheException{
        for (ProgramSchema ps : fileMap.programs) {
            if (ps.name.compareToIgnoreCase(name) == 0) {
                throw new VMCacheException("Program with name: "+name+" already exists!");
            }
        }

        ProgramSchema toadd = new ProgramSchema(name, main);
        fileMap.programs.add(toadd);

        try {
            unpackJar(name);
            mapper.writeValue(new File(ConfigLoader.STORAGE_DIR + "/programs.json"), fileMap);
        } catch (IOException e) {
            throw new VMCacheException("Failed to update programs json... "+e);
        }
    }

    /**
     * Initializes the directories required
     */
    private void initializeDirectories() {
        String root = ConfigLoader.STORAGE_DIR;
        File rootPath = new File(root);
        File config = new File(root+"/programs.json");
        File jarPath = new File(root+"/jars");
        File unpackedPath = new File(root+"/unpacked");

        if (Files.notExists(rootPath.toPath())) {
            if (!rootPath.mkdirs()) {
                System.err.println("[CRITICAL ERROR] Failed to mkdirs in path: "+root);
                System.exit(3);
            }
        }

        try {
            if (config.createNewFile()) {
                try (PrintWriter pw = new PrintWriter(config)) {
                    pw.println("{ \"programs\": [] }");
                }
            }
        }
        catch (IOException e) {
            System.err.println("[CRITICAL ERROR] Failed to touch program file in: "+root+" "+e);
            System.exit(3);
        }

        if (Files.notExists(jarPath.toPath())) {
            if (!jarPath.mkdirs()) {
                System.err.println("[CRITICAL ERROR] Failed to mkdirs in path: "+root);
                System.exit(3);
            }
        }

        if (Files.notExists(unpackedPath.toPath())) {
            if (!unpackedPath.mkdirs()) {
                System.err.println("[CRITICAL ERROR] Failed to mkdirs in path: "+root);
                System.exit(3);
            }
        }
    }

    /**
     * Gets the singleton object (or creates it)
     * @return Cache object
     */
    public static VMCache getSingleton() {
        return VMCache.singleton == null ? new VMCache() : VMCache.singleton;
    }
}
