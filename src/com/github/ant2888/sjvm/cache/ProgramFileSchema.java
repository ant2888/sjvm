package cache;

import java.util.ArrayList;
import java.util.Arrays;

public class ProgramFileSchema {
    public ArrayList<ProgramSchema> programs;

    public ProgramFileSchema(ProgramSchema... programs) {
        this.programs = new ArrayList<>();
        this.programs.addAll(Arrays.asList(programs));
    }

    public ProgramFileSchema() {}

    public ArrayList<ProgramSchema> getPrograms() {
        return programs;
    }

    public void setPrograms(ArrayList<ProgramSchema> programs) {
        this.programs = programs;
    }
}
