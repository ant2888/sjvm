package cache;

public class VMCacheException extends Exception{

    public VMCacheException(String problem){
        super(problem);
    }

}
