package cache;

public class ProgramSchema {

    public String name, main;

    public ProgramSchema(String name, String main) {
        this.name = name;
        this.main = main;
    }

    public ProgramSchema() {}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
