package utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Class to load a config file in
 */
public class ConfigLoader {

    private static ConfigLoader singleton;
    private Properties curProps;

    public static String CONFIG_LOCATION = null;
    public static String STORAGE_DIR = System.getenv("user.home") + "/.sjvm";
    public static int THREAD_POOL = 2;

    private ConfigLoader() {
        ConfigLoader.singleton = this;
    }

    /**
     * Gets the properties of the given config file (specified via cli)
     * @return A properties object that represent the mapping conf_key->val
     */
    public Properties getConfigProperties() {
        if (curProps != null) return curProps;
        if (CONFIG_LOCATION == null || CONFIG_LOCATION.compareToIgnoreCase("") == 0) {
            curProps = ConfigLoader.getDefaultProperties();
            fillConfig();
            return curProps;
        }

        InputStream is = getClass().getClassLoader().getResourceAsStream(ConfigLoader.CONFIG_LOCATION);
        Properties toRet = new Properties();

        if (is != null) {
            try {
                toRet.load(is);
            } catch(IOException e){
                System.err.println("[FAILED TO LOAD CONFIG] "+e);
            }
            curProps = toRet;
        }
        else {
            System.err.println("[BAD CONFIG FILE] Going default");
            curProps = ConfigLoader.getDefaultProperties();
        }

        fillConfig();
        return curProps;
    }

    private void fillConfig() {
        String tmp;

        STORAGE_DIR = curProps.getProperty("storage_dir", STORAGE_DIR);
        if ((tmp = curProps.getProperty("thread_pool", "")).compareToIgnoreCase("") != 0) {
            THREAD_POOL = Integer.parseInt(tmp);
        }
    }

    /**
     * Sets the default properties for the program in the case of the config being missing
     * @return The properties map of the default properties
     */
    private static Properties getDefaultProperties() {
        Properties toRet = new Properties();
        String usrHome = System.getProperty("user.home");

        toRet.put("storage_dir", usrHome+"/.sjvm");
        toRet.put("thread_pool", "2");

        return toRet;
    }

    /**
     * Retrieves the singleton object else creates it
     * @return The config loader instance
     */
    public static ConfigLoader getSingleton() {
        return singleton != null ? singleton : new ConfigLoader();
    }
}
