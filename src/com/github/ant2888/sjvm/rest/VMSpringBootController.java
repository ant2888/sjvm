package rest;

import org.springframework.boot.autoconfigure.*;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@EnableAutoConfiguration
public class VMSpringBootController {

    @RequestMapping("/")
    String root() {
        return "Testing Server\n";
    }


}
