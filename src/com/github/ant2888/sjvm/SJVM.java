
import cache.VMCache;
import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.helper.HelpScreenException;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;
import org.springframework.boot.SpringApplication;
import rest.VMSpringBootController;
import utils.ConfigLoader;

/**
 * Main class for the SJVM
 */
public class SJVM {

    /**
     * Entry point for the SJVM. Launches spring-boot, initializes configs/pre-downloaded JARS, etc
     * @param args Arguments for the SJVM
     */
	public static void main(String[] args) {
        ArgumentParser parser = ArgumentParsers.newFor("sjvm").build().description("Launches the SJVM");

        parser.addArgument("--spring-config")
                .dest("springConf")
                .setDefault("./")
                .help("location of the spring conf file");
        parser.addArgument("-c", "--config")
                .dest("sjvmConf")
                .setDefault("")
                .help("location of the sjvm conf file");

        Namespace res = null;
        try {
            res = parser.parseArgs(args);
        } catch (ArgumentParserException e) {
            parser.handleError(e);
            if (e instanceof HelpScreenException)
                System.exit(0);
            System.exit(1);
        } finally {
            if (res == null) System.exit(2);
        }

        ConfigLoader.CONFIG_LOCATION = res.get("sjvmConf");
        ConfigLoader.getSingleton().getConfigProperties();

        VMCache.getSingleton();

        String[] arr = {
                "--spring.config.location="+res.get("springConf")
        };
        SpringApplication.run(VMSpringBootController.class, arr);
	}

}
