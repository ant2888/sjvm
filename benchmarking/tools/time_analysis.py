import argparse
import sys

def get_time(jarfile):
    print(f'Jar file received {jarfile}')


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('jarfile', type=str, help='The built jar file')
    get_time(parser.parse_args().jarfile)
