package com.github.ant2888.jvmstress;

public class jvmstress {

    public static void main(String[] args){
        // this is a simple java test to make sure we are seeing any results
        try {
            Thread.sleep(5000);
        } catch (Exception e){
            System.out.println("Failed to sleep -- "+e);
        }
    }

}
